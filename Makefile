SCRIPTS=$(shell find source/*)

DESTDIR=
PREFIX=usr/local

RGCC=build/rgcc
RGXX=build/rg++

.PHONY: all clean install uninstall

all: build/rgcc build/rg++
	@echo "All prepared"

$(RGCC): source/rgcc source/main_part.sh
	mkdir -p build/
	cat $^ > $@
	chmod +x $@

$(RGXX): source/rg++ source/main_part.sh
	mkdir -p build/
	cat $^ > $@
	chmod +x $@

clean:
	rm -rf build/

install:
	mkdir -p $(DESTDIR)/$(PREFIX)/bin/
	cp $(RGCC) $(DESTDIR)/$(PREFIX)/bin/
	cp $(RGXX) $(DESTDIR)/$(PREFIX)/bin/

uninstall:
	rm -f $(DESTDIR)/$(PREFIX)/bin/rgcc
	rm -f $(DESTDIR)/$(PREFIX)/bin/rg++
