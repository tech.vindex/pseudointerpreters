#!/usr/bin/env -S rg++ -lglut -lGLU -lGL -lftgl
#include <GL/glut.h>
#include <math.h>
#include <vector>

const double globalWinWidth  = 320;
const double globalWinHeight = 240;
const int globalRefreshPeriod = 7;


class Circle {

public:

    Circle(int x=0, int y=0, int r=10) {
        _x = x;
        _y = y;
        _r = r;
    };

    void draw() {
        glColor3f(1, 0, 1);
        glBegin(GL_LINE_LOOP);
        for (double i = 0; i < 2*M_PI; i += M_PI/18) {
            glVertex2f(_x + _r*sin(i), _y + _r * cos(i));
        }
        glEnd();
    }
    
    void changeCoord(int x=0, int y=0) {
        _x = x;
        _y = y;
    };

private:

    int _x, _y, _r;

};


class PositionPointer {

public:

    PositionPointer(int center_x, int center_y, int length,
                    double angle, int refresh) {
        cx = center_x;
        cy = center_y;
        l  = length;
        rad_angle = angle;
        refreshPeriod = refresh;
        circle = new Circle(l*sin(rad_angle), -l*cos(rad_angle), 3);
    }

    void changeAngle(double newAngle) {
        rad_angle = newAngle;
    }

    void draw() {
        glLineWidth(3);
        circle->changeCoord(l*sin(rad_angle), -l*cos(rad_angle));
        circle->draw();
    }

private:

    int cx, cy, l;
    double rad_angle;
    int refreshPeriod;
    Circle* circle;
};


PositionPointer arrow(0, 0, 66, 6, globalRefreshPeriod);


void draw() {
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.2, 0.2, 0.4);
    glutSolidSphere(150/2, 45, 15);
    glColor3f(0, 0, 0);
    glutSolidSphere(147/2, 45, 15);
    arrow.draw();
    glutSwapBuffers();
}


void timer(int value) {
    const int fullPointerTime = 1000;
    glutTimerFunc(globalRefreshPeriod, &timer, 0);
    
    static int circle_time = fullPointerTime;
    arrow.changeAngle(2*M_PI*(fullPointerTime - circle_time)/fullPointerTime);
    glutPostRedisplay();
    circle_time -= globalRefreshPeriod;
    if (circle_time <= 0)
        circle_time = fullPointerTime;
}


void initialize() {
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(
        -globalWinWidth/2,
        globalWinWidth/2,
        globalWinHeight/2,
        -globalWinHeight/2,
        -200,
        200
    );
    glMatrixMode(GL_MODELVIEW);
}


int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(globalWinWidth, globalWinHeight);
    glutInitWindowPosition(100, 200);
    glutCreateWindow("OpenGL");
    glutDisplayFunc(draw);
    glutTimerFunc(0, timer, 0);
    initialize();
    glutMainLoop();
    return 0;
}

