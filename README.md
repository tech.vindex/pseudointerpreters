## pseudointerpreters

This project provides rgcc and rg++ scripts for implicitly compiling and running C/C++ programs. Scripts can only work with single-file sources.

---

## Ready-made packages

See [download page](https://gitlab.com/tech.vindex/pseudointerpreters/wikis/Download-page).

---

## Build from source and installation


Creating of executable scripts:

`$ make`

Installation:

`# make install`

Uninstalling installed scripts:

`# make uninstall`

---

## Usage

`$ rgcc <options for gcc> <file>.c`

`$ rg++ <options for g++> <file>.cpp`

We also can use she-bang.
There are examples of such usage in the `samples/` directory.

For example, she-bang for OpenGL-based C++ programs:

`#!/usr/local/bin/rg++ -lglut -lGLU -lGL -lftgl`

or (better)

`#!/usr/bin/env -S rg++ -lglut -lGLU -lGL -lftgl`

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---
