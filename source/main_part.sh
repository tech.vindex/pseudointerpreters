VERSION=0.4

if [[ $# -eq 1 && "$1" == "--version" ]]; then
    echo $VERSION
    exit
fi

CODE_FILE=${@:$#:1}  # last argument
DEVICE_AND_INODE=`stat -c "%d.%i" "$CODE_FILE"`
BASE_NAME=`basename "$CODE_FILE"`
BIN_NAME=${BASE_NAME%.*}.${DEVICE_AND_INODE}
let "LAST_INDEX=$#-1"

TEMP_DIR="/tmp/${RCC}"
mkdir -p "/tmp/${RCC}"
TEMP_FILE="${TEMP_DIR}/${BASE_NAME}"

FIRST_LINE=`head -n 1 "$CODE_FILE"`
if [[ "$FIRST_LINE" == '#!'* ]]; then
    tail -n +2 "$CODE_FILE" > "$TEMP_FILE"
    sed  -i  '1 s/^/\n/' "$TEMP_FILE"
else
    cp "$CODE_FILE" "$TEMP_FILE"
fi

BIN="${TEMP_DIR}/${BIN_NAME}"
if [[ -e "$BIN" ]]; then
    CODE_FILE_TIME=$(stat -c "%Y" "${CODE_FILE}")
    BIN_FILE_TIME=$(stat -c "%Y" "${BIN}")
    if [[ $BIN_FILE_TIME -gt $CODE_FILE_TIME ]]; then
        "${BIN}"
        exit
    fi
fi

"${CC}" "${TEMP_FILE}" -o "${BIN}" ${@: 1:$LAST_INDEX}
RET=$?
rm "$TEMP_FILE"
if [[ $RET -eq 0 ]]; then
    "${BIN}"
fi

